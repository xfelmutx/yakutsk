package com.example.test;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class PagerAdapter extends RecyclerView.Adapter<PagerAdapter.ViewHolder> {
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.
                        from(parent.getContext())
                        .inflate(R.layout.page, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == 0) {
            holder.itemView.findViewById(R.id.button_1).setVisibility(View.INVISIBLE);
            holder.itemView.findViewById(R.id.button_2).setVisibility(View.INVISIBLE);
            ((TextView) holder.itemView.findViewById(R.id.text)).setText("1");

        } else {
//            holder.itemView.findViewById(R.id.button_2).setVisibility(View.INVISIBLE);
            ((TextView) holder.itemView.findViewById(R.id.text)).setText("2");
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}